package app.controllers;

import app.ApiClient;
import app.SortOrder;
import app.Utils;
import app.models.Chat;
import app.models.ChatUser;
import app.models.Message;
import app.requests.MessageRequest;
import app.requests.NewChatRequest;
import app.requests.NewMessageRequest;
import app.responses.ChatResponse;
import com.google.gson.Gson;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/web")
public class WebRestController {
    @Value("${constants.router_url}")
    private String routerUrl;
    private static final Logger logger = LogManager.getLogger(Class.class.getName());
    private ApiClient chatApi;
    private ApiClient authApi;
    private ApiClient messageApi;

    @Autowired
    private HttpServletRequest request;

    @PostConstruct
    private void init() {
        this.chatApi = new ApiClient(this.routerUrl + "/chat");
        this.authApi = new ApiClient(this.routerUrl + "/auth");
        this.messageApi = new ApiClient(this.routerUrl + "/message");
    }

    @GetMapping("")
    public ModelAndView client(ModelMap model) {
        logger.info("/client");
        model.addAttribute("url", routerUrl);
        return new ModelAndView("client", model);
    }

    @GetMapping("/user")
    @HystrixCommand(fallbackMethod = "fallback", commandProperties = {
            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "15000")
    })
    public String getUserId() {
        String username = get(authApi, "/user");
        return username;
    }

    @GetMapping("/chat")
    @HystrixCommand(fallbackMethod = "fallback", commandProperties = {
            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "30000"),
    })
    public String chat() {
        String chat = get(this.chatApi, "/random");
        System.out.println(chat);
        return chat;
    }

    @HystrixCommand(fallbackMethod = "fallback", commandProperties = {
            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "15000000")
    })
    @PostMapping("/chat/{chatId}")
    public String enterChat(@PathVariable int chatId, @RequestBody ChatUser user,
                            @RequestParam(value = "page", defaultValue = "1") int page,
                            @RequestParam(value = "size", defaultValue = "10") int size,
                            @RequestParam(value = "sort", defaultValue = "asc") SortOrder sorting) {
        String chatResp = get(this.chatApi, "/" + chatId);
        Chat chat = new Gson().fromJson(chatResp, Chat.class);
        String resp = get(chatApi, "/" + chatId + "/messages");

        List<Message> messages = getGson().fromJson(resp, Message.MessageList.class);
        int totalMessages = messages.size();
        messages = Utils.sortMessages(messages, sorting, page, size);

        int responsePage = (int) Math.ceil((float) totalMessages / size);
        return new Gson().toJson(new ChatResponse(chat, messages, responsePage));
    }

    @HystrixCommand(fallbackMethod = "chat_post_fallback", commandProperties = {
            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "15000")
    })
    @PostMapping("/chat/{chatId}/send")
    public ResponseEntity<String> sendMessage(@PathVariable Long chatId,
                                              @RequestBody MessageRequest request) {
        logger.info("/chat/" + chatId + "/send");
        NewMessageRequest messageRequest = new NewMessageRequest();
        messageRequest.chat_id = chatId;
        messageRequest.message.text = request.getMessage();
        messageRequest.message.userId = request.getUser();
        post(messageApi, "/", messageRequest.toJson());
        return new ResponseEntity<>("ok", HttpStatus.OK);
    }

    @HystrixCommand(fallbackMethod = "fallback", commandProperties = {
            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "15000")
    })
    @PostMapping("/chat/new")
    public String createChat(@RequestBody NewChatRequest request) {
        logger.info("/chat/new");
        String chatResponse = post(chatApi, "/", request.toJson());
        Chat chat = getGson().fromJson(chatResponse, Chat.class);
        MessageRequest messageRequest = new MessageRequest();
        messageRequest.setMessage("Chat #" + chat.getChatId() + " " + chat.getTitle() + " has been created");
        messageRequest.setUser(this.getUserId());
        this.sendMessage(chat.getChatId(), messageRequest);
        return chatResponse;
    }

    private String post(ApiClient client, String url, String payload) {
        client.setRequest(request);
        return client.post(url, payload);
    }

    private String get(ApiClient client, String url) {
        client.setRequest(request);
        return client.get(url);
    }

    private Gson getGson() {
        return new Gson();
    }

    private String fallback() {
        return "Timeout";
    }

    private String fallback(NewChatRequest request) {
        return "Timeout:" + request.toString();
    }

    private String fallback(int chatId, long messageId) {
        return "Timeout:" + chatId + ":" + messageId;
    }

    private ResponseEntity<String> chat_post_fallback(int chatId, MessageRequest request) {
        return new ResponseEntity<>("Timeout", HttpStatus.GATEWAY_TIMEOUT);
    }

    private ResponseEntity<String> chat_edit_fallback(int chatId, long messageId, MessageRequest request) {
        return new ResponseEntity<>("Timeout", HttpStatus.GATEWAY_TIMEOUT);

    }

    private String fallback(int chatId, ChatUser user, int page, int size, SortOrder sorting) {
        return "Timeout:" + chatId + ":" + user.toString();
    }
}
