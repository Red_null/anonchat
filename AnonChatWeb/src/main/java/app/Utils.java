package app;

import app.models.Message;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;


public class Utils {
    public static List<Message> sortMessages(List<Message> messages, SortOrder sorting, int page, int size) {
        if (sorting == SortOrder.dsc) {
            Collections.reverse(messages);
        }
        int startIdx = (page - 1) * size;
        if (startIdx > messages.size()) {
            messages = new LinkedList<>();
        } else {
            int endIdx = Math.min(startIdx + size, messages.size());
            messages = new LinkedList<>(messages.subList(startIdx, endIdx));
        }
        return messages;
    }
}
