package app.responses;

import app.models.Chat;
import app.models.Message;

import java.util.LinkedList;
import java.util.List;

public class ChatResponse {
    private Chat chat;
    private int pages;
    private List<Message> messages;

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public ChatResponse(Chat chat, List<Message> messages, int pages) {
        this.chat = chat;
        this.messages = messages;
        this.pages = pages;
    }


    public Chat getChat() {
        return chat;
    }

    public void setChat(Chat chat) {
        this.chat = chat;
    }

    public List<Message> getMessages() {
        return messages;
    }

    public void setMessages(LinkedList<Message> messages) {
        this.messages = messages;
    }
}
