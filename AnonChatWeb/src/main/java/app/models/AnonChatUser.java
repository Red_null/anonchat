package app.models;

import lombok.Data;

@Data
public abstract class AnonChatUser {
    public String id;
}
