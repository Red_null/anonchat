package app.models;

import com.google.gson.Gson;
import lombok.Data;

import java.util.ArrayList;

@Data
public class Chat {
    private Long chatId;
    private String title;

    public static class ChatList extends ArrayList<Chat> {
    }

    public String toJson() {
        return new Gson().toJson(this);
    }
}