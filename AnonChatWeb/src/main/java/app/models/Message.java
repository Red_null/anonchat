package app.models;

import lombok.Data;

import java.time.Instant;
import java.util.ArrayList;

@Data
public class Message {
    private long id;
    private String text;
    private String timestamp;
    private String userId;
    private long chatId;

    public static class MessageList extends ArrayList<Message> {
    }

    public Message(String userId, String text, long chat_id) {
        this.userId = userId;
        this.text = text;
        this.chatId = chat_id;
        this.timestamp = Instant.now().toString();
    }

    public void update(String newText) {
        this.text = newText;
        this.timestamp = Instant.now().toString();
    }
}
