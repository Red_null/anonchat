package app.requests;

import lombok.Data;

@Data
public class MessageRequest {
    private String user;
    private String message;
}
