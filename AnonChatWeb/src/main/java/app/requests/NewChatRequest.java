package app.requests;

import com.google.gson.Gson;
import lombok.Data;

@Data
public class NewChatRequest {
    private String title;

    public String toJson() {
        return new Gson().toJson(this);
    }
}
