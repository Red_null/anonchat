package app.requests;

import com.google.gson.Gson;
import lombok.Data;

@Data
public class NewMessageRequest {
    public long chat_id;
    public MessageInner message;

    public NewMessageRequest() {
        this.message = new MessageInner();
    }

    public String toJson() {
        return new Gson().toJson(this);
    }

    @Data
    public static class MessageInner {
        public String text;
        public String userId;
    }
}