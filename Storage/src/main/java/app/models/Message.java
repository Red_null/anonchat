package app.models;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class Message extends SerializableModel {
    public Message() {
    }

    public Message(String text, String timestamp, String userId, Chat chat) {
        this.text = text;
        this.timestamp = timestamp;
        this.userId = userId;
        this.chat = chat;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String text;
    private String timestamp;
    private String userId;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "chat_id", nullable = false)
    private Chat chat;

    public void update(Message message) {
        this.text = message.text;
        this.timestamp = message.timestamp;
    }
}
