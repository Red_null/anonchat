package app.models;

import app.utils.HibernateProxyTypeAdapter;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

abstract class SerializableModel {
    public String toJson() {
        return this.getGson().toJson(this);
    }
    
    public Gson getGson() {
        return this.getGsonBuilder().create();
    }

    protected GsonBuilder getGsonBuilder() {
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);
        return builder;
    }
}
