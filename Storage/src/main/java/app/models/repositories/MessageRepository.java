package app.models.repositories;

import app.models.Chat;
import app.models.Message;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.LinkedList;

@Repository
public interface MessageRepository extends CrudRepository<Message, Long> {
    LinkedList<Message> getAllByChatChatId(long chatId);

    Message getById(long messageId);

    LinkedList<Message> getAllByChatOrderByTimestamp(Chat chat);
}
