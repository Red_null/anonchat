package app.models;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@Entity
public class Chat extends SerializableModel {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long chatId;
    private String title;
}

