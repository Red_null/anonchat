package app;

import app.requests.NewMessageRequest;
import com.google.gson.Gson;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RabbitClient {
    @Autowired
    MessageManager manager;

    private final static String MESSAGE_QUEUE_NAME = "message";
    private ConnectionFactory factory;

    RabbitClient() {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("rabbit");
        factory.setUsername("rabbitmq");
        factory.setPassword("rabbitmq");
        this.factory = factory;
    }

    public void runConsumers() {
        consume(MESSAGE_QUEUE_NAME);
    }

    private void consume(String queue) {
        try {
            Connection connection = factory.newConnection();
            Channel channel = connection.createChannel();

            channel.queueDeclare(queue, false, false, false, null);
            System.out.println(" [*] Waiting for messages");

            DeliverCallback deliverCallback = (consumerTag, delivery) -> {
                String message = new String(delivery.getBody(), "UTF-8");
                System.out.println(" [x] Received '" + message + "'");

                NewMessageRequest request = new Gson().fromJson(message, NewMessageRequest.class);
                manager.addMessage(request);
            };

            channel.basicConsume(queue, true, deliverCallback, consumerTag -> {
            });

        } catch (Exception e) {
            System.err.println(e.toString());
        }
    }

}

