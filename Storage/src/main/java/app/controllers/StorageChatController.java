package app.controllers;

import app.models.Chat;
import app.models.Message;
import app.models.repositories.ChatRepository;
import app.models.repositories.MessageRepository;
import app.requests.NewChatRequest;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/storage")
public class StorageChatController {
    @Autowired
    ChatRepository repository;

    @Autowired
    MessageRepository messageRepository;

    @GetMapping
    public String index() {
        return "index";
    }

    @GetMapping("/chats")
    public String chats() {
        List<Chat> chats = (List<Chat>) repository.findAll();
        return new Gson().toJson(chats);
    }

    @PostMapping("/chats")
    public String addChat(@RequestBody NewChatRequest chatRequest) {
        Chat chat = new Chat();
        chat.setTitle(chatRequest.title);
        repository.save(chat);
        return chat.toJson();
    }

    @GetMapping("/chats/{chatId}")
    public String chat(@PathVariable long chatId) {
        Chat chat = repository.findByChatId(chatId);
        return chat.toJson();
    }

    @GetMapping("/chats/{chatId}/messages")
    public String chatMessages(@PathVariable long chatId) {
        Chat chat = repository.findByChatId(chatId);
        List<Message> messages = messageRepository.getAllByChatChatId(chat.getChatId());
        return new Gson().toJson(messages);
    }

    @PatchMapping("/chats/{chatId}")
    public String chatUpdate(@PathVariable long chatId, @RequestBody Chat chat) {
        chat.setChatId(chatId);
        repository.save(chat);
        return chat.toJson();
    }
}
