package app.controllers;

import app.RabbitClient;
import app.models.Chat;
import app.models.Message;
import app.models.repositories.ChatRepository;
import app.models.repositories.MessageRepository;
import app.requests.NewMessageRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;

@RestController
@RequestMapping("/storage")
public class StorageMessageController {

    @Autowired
    RabbitClient rabbitClient;

    @Autowired
    MessageRepository repository;

    @Autowired
    ChatRepository chatRepository;

    @GetMapping("/messages/{messageId}")
    public String message(@PathVariable long messageId) {
        Message message = repository.getById(messageId);
        return message.toJson();
    }

    @PostConstruct
    private void init() {
        rabbitClient.runConsumers();
    }

    @PostMapping("/messages")
    public String addMessage(@RequestBody NewMessageRequest request) {
        Chat chat = chatRepository.findByChatId(request.chat_id);
        NewMessageRequest.MessageInner newMessage = request.message;
        Message message = new Message(newMessage.text, newMessage.timestamp, newMessage.userId, chat);
        repository.save(message);
        return message.toJson();
    }

    @PatchMapping("/messages/{messageId}")
    public String editMessage(@PathVariable long messageId, @RequestBody Message newMessage) {
        Message message = repository.getById(messageId);
        message.update(newMessage);
        repository.save(message);
        return message.toJson();
    }

}
