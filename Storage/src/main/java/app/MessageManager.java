package app;

import app.models.Chat;
import app.models.Message;
import app.models.repositories.ChatRepository;
import app.models.repositories.MessageRepository;
import app.requests.NewMessageRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

@Service
public class MessageManager {
    @Autowired
    MessageRepository repository;

    @Autowired
    ChatRepository chatRepository;


    public String addMessage(@RequestBody NewMessageRequest request) {
        Chat chat = chatRepository.findByChatId(request.chat_id);
        NewMessageRequest.MessageInner newMessage = request.message;
        Message message = new Message(newMessage.text, newMessage.timestamp, newMessage.userId, chat);
        repository.save(message);
        return message.toJson();
    }
}
