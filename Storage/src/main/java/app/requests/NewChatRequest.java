package app.requests;

import lombok.Data;

@Data
public class NewChatRequest {
    public String title;
}
