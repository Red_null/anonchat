package app.requests;

import lombok.Data;

@Data
public class NewMessageRequest {
    public long chat_id;
    public MessageInner message;

    @Data
    public static class MessageInner {
        public String text;
        public String timestamp;
        public String userId;
    }
}
