package app.controllers;

import app.ApiClient;
import app.Models.Chat;
import app.Requests.NewChatRequest;
import com.google.gson.Gson;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Random;


@RestController
@RequestMapping("/chat")
public class ChatController { //todo add chat pool
    @Value("${constants.router_url}")
    private String routerUrl;

    @Autowired
    private HttpServletRequest request;

    private ApiClient storageApi;

    @PostConstruct
    private void init() {
        this.storageApi = new ApiClient(this.routerUrl + "/storage");
    }

    @HystrixCommand(fallbackMethod = "fallback", commandProperties = {
            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "13000")
    })
    @GetMapping("/random")
    public String randomChat() {
        String response = get(storageApi, "/chats");
        List<Chat> chats = getGson().fromJson(response, Chat.ChatList.class);
        Chat randomChat = chats.get(new Random().nextInt(chats.size()));
        return getGson().toJson(randomChat);
    }

    @HystrixCommand(fallbackMethod = "fallback", commandProperties = {
            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "13000")
    })
    @GetMapping("/{chatId}")
    public String getChat(@PathVariable String chatId) {
        return get(storageApi, "/chats/" + chatId);
    }

    @HystrixCommand(fallbackMethod = "fallback", commandProperties = {
            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "13000")
    })
    @GetMapping("/{chatId}/messages")
    public String getChatMessages(@PathVariable String chatId) {
        return get(storageApi, "/chats/" + chatId + "/messages");
    }

    @HystrixCommand(fallbackMethod = "fallback", commandProperties = {
            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "13000")
    })
    @PostMapping("/")
    public String createChat(@RequestBody NewChatRequest chat) {
        String payload = chat.toJson();
        return post(storageApi, "/chats", payload);
    }

    private String post(ApiClient client, String url, String payload) {
        client.setRequest(request);
        return client.post(url, payload);
    }

    private String get(ApiClient client, String url) {
        client.setRequest(request);
        return client.get(url);
    }

    private Gson getGson() {
        return new Gson();
    }

    private String fallback() {
        return "Timeout";
    }

    private String fallback(Chat chat) {
        return "Timeout";
    }


    private String fallback(String chatId) {
        return "Timeout:" + chatId;
    }

}
