package app;

import org.springframework.http.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;

public class ApiClient {
    private String baseUrl;
    private HttpServletRequest request = null;

    public ApiClient(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public void setRequest(HttpServletRequest request) {
        this.request = request;
    }

    private String getAuthToken() {
        System.out.println(request.getHeader("authorization"));
        return request.getHeader("authorization").replace("Bearer ", "");
    }

    private HttpEntity<String> getRequest(HttpHeaders headers, String body) {
        headers.setBearerAuth(getAuthToken());
        return new HttpEntity<>(body, headers);
    }

    private HttpEntity<String> getRequest() {
        HttpHeaders headers = new HttpHeaders();
        return getRequest(headers, "");
    }


    public String get(String url) {
        url = this.baseUrl + url;
        System.out.println("----GET----");
        System.out.println(url);
        ResponseEntity<String> response = new RestTemplate().exchange(url, HttpMethod.GET, getRequest(), String.class);
        this.checkStatus(response);
        return response.getBody();
    }

    public String post(String url, String payload) {
        url = this.baseUrl + url;
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setBearerAuth(getAuthToken());

        System.out.println("----POST----");
        System.out.println(url);
        System.out.println(payload);

        HttpEntity<String> request = getRequest(headers, payload);
        ResponseEntity<String> response = new RestTemplate().postForEntity(url, request, String.class);
        this.checkStatus(response);
        return response.getBody();
    }

    private void checkStatus(ResponseEntity response) {
        HttpStatus statusCode = response.getStatusCode();
        if (statusCode != HttpStatus.OK) {
            throw new HttpClientErrorException(statusCode);
        }
    }
}
