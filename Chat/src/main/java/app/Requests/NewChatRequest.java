package app.Requests;

import com.google.gson.Gson;
import lombok.Data;

@Data
public class NewChatRequest {
    public String title;

    public String toJson() {
        return new Gson().toJson(this);
    }

}
