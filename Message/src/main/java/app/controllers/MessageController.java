package app.controllers;

import app.ApiClient;
import app.RabbitClient;
import app.models.Message;
import app.requests.NewMessageRequest;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/message")
public class MessageController {

    @Value("${constants.router_url}")
    private String routerUrl;

    private ApiClient storageApi;

    @Autowired
    private HttpServletRequest request;

    @PostConstruct
    private void init() {
        this.storageApi = new ApiClient(this.routerUrl + "/storage");
    }

    @PostMapping("/")
    @HystrixCommand(fallbackMethod = "fallback", commandProperties = {
            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "15000")
    })
    public String newMessage(@RequestBody NewMessageRequest request) {
        Message message = new Message(request.message.userId, request.message.text, request.chat_id);
        NewMessageRequest storageRequest = new NewMessageRequest(message);
        RabbitClient client = new RabbitClient();
        client.send(storageRequest.toJson(), RabbitClient.MESSAGE_QUEUE_NAME);
        System.out.println("SEND_TO_RABBIT");
        return "OK";
    }

    private String fallback(NewMessageRequest request) {
        return "Timeout";
    }

    private String post(ApiClient client, String url, String payload) {
        client.setRequest(request);
        return client.post(url, payload);
    }

    private String get(ApiClient client, String url) {
        client.setRequest(request);
        return client.get(url);
    }
}
