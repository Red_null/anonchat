package app.requests;

import app.models.Message;
import com.google.gson.Gson;
import lombok.Data;

@Data
public class NewMessageRequest {
    public long chat_id;
    public MessageInner message;

    @Data
    public static class MessageInner {
        public String text;
        public String userId;
        public String timestamp;
    }

    public NewMessageRequest(){
        this.message = new MessageInner();
    }

    public NewMessageRequest(Message message) {
        this.chat_id = message.getChatId();
        this.message = new MessageInner();
        this.message.text = message.getText();
        this.message.userId = message.getUserId();
        this.message.timestamp = message.getTimestamp();
    }

    public String toJson() {
        return new Gson().toJson(this);
    }
}
