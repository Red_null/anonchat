package app;


import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

public class RabbitClient {
    public final static String MESSAGE_QUEUE_NAME = "message";

    public void send(String payload, String queueName) {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("rabbit");
        factory.setUsername("rabbitmq");
        factory.setPassword("rabbitmq");
        try {
            Connection connection = factory.newConnection();
            Channel channel = connection.createChannel();
            channel.queueDeclare();
            channel.basicPublish("", queueName, null, payload.getBytes());

        } catch (Exception e) {
            System.err.println(e.toString());
        }
    }
}
