package app;

import app.configs.JwtConfig;
import app.user.UserCreds;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Date;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class JwtAuthFilter extends UsernamePasswordAuthenticationFilter {

    // We use auth manager to validate the user credentials
    private AuthenticationManager authManager;

    private final JwtConfig jwtConfig;

    public JwtAuthFilter(AuthenticationManager authManager, JwtConfig jwtConfig) {
        this.authManager = authManager;
        this.jwtConfig = jwtConfig;

        // By default, UsernamePasswordAuthenticationFilter listens to "/login" path.
        // In our case, we use "/auth". So, we need to override the defaults.
        this.setRequiresAuthenticationRequestMatcher(new AntPathRequestMatcher(jwtConfig.getUri(), "POST"));
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
            throws AuthenticationException {

        try {
            // 1. Get credentials from request
            UserCreds creds = new ObjectMapper().readValue(request.getInputStream(), UserCreds.class);

            // 2. Create auth object (contains credentials) which will be used by auth manager
            UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(
                    creds.login, creds.password, Collections.emptyList());
            // 3. Authentication manager authenticate the user, and use UserDetialsServiceImpl::loadUserByUsername() method to load the user.
            return authManager.authenticate(authToken);

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    // Upon successful authentication, generate a token.
    // The 'auth' passed to successfulAuthentication() is the current authenticated user.
    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
                                            Authentication auth) {
        long now = System.currentTimeMillis();
        Stream<String> authorities = auth.getAuthorities().stream().map(GrantedAuthority::getAuthority);

        List<String> authoritiesList = authorities.collect(Collectors.toList());
        Date expiresAt = new Date(now + jwtConfig.getExpiration() * 1000);

        JwtBuilder jwt = Jwts.builder()
                .setSubject(auth.getName())
                .claim("authorities", authoritiesList)
                .setIssuedAt(new Date(now))
                .setExpiration(expiresAt)
                .signWith(SignatureAlgorithm.HS512, jwtConfig.getSecret().getBytes());

        String token = jwtConfig.getPrefix() + jwt.compact();
        response.addHeader(jwtConfig.getHeader(), token);

        try {
            response.getWriter().write(token);
            response.getWriter().close();
            response.getWriter().flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}