package app;

import app.configs.JwtConfig;
import app.user.User;
import app.user.UserCreds;
import app.user.UserRepository;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/auth")
public class AuthController {
    @Autowired
    UserRepository userRepository;

    @Autowired
    BCryptPasswordEncoder encoder;

    @Autowired
    HttpServletRequest request;
    @Autowired
    JwtConfig jwtConfig;

    @PostMapping("/register")
    public ResponseEntity<HttpStatus> register(@RequestBody UserCreds userRequest) {
        String password = encoder.encode(userRequest.password);
        User user = new User(userRequest.login, password);
        userRepository.save(user);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/user")
    public String getUserName() {
        String authToken = getAuthToken();
        Claims claims = Jwts.parser()
                .setSigningKey(jwtConfig.getSecret().getBytes())
                .parseClaimsJws(authToken)
                .getBody();

        String user = claims.getSubject();
        return user;
    }

    private String getAuthToken() {
        return request.getHeader("authorization").replace("Bearer ", "");
    }
}
