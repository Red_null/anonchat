package app.configs;

import org.springframework.beans.factory.annotation.Value;

import lombok.Getter;
import lombok.ToString;

 @Getter
 @ToString
public class JwtConfig {

	@Value("${security.jwt.uri:/auth/**}")
    private String Uri;

    @Value("${security.jwt.header:Authorization}")
    private String header;

    @Value("${security.jwt.prefix:Bearer }")
    private String prefix;

    @Value("${security.jwt.expiration:#{48*60*60}}")
    private int expiration;

    @Value("${security.jwt.secret:JwtSecretKey}")
    private String secret;

    @Value("${security.oauth2.client.user-header}")
    private String userHeader;
}
