package app.user;


import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.Collection;
import java.util.Collections;


@Entity
@Table(name = "users")
public class User implements UserDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;

    @Column(name = "USERNAME", unique = true)
    public String username;

    @Column(name = "PASSWORD", nullable = false)
    public String password;

    @Column(name = "ENABLED", nullable = false)
    public boolean enabled;

    // Remember that Spring needs roles to be in this format: "ROLE_" + userRole (i.e. "ROLE_ADMIN")
    // So, we need to set it to that format, so we can verify and compare roles (i.e. hasRole("ADMIN")).
    public String role;

    User() {
        this.role = "USER";
        this.enabled = true;
    }

    public User(String username, String password) {
        this(username, password, true, "USER");
    }

    User(String username, String password, boolean enabled, String role) {
        super();
        this.username = username;
        this.password = password;
        this.enabled = enabled;
        this.role = role;
    }

    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    public boolean isEnabled() {
        return enabled;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.singletonList(new SimpleGrantedAuthority(this.getFormattedRole()));
    }

    public String getPassword() {
        return password;
    }

    public String getFormattedRole() {
        return "ROLE_" + this.role;
    }
}